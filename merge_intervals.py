
def merge_pair(pair: list):
    """
    Takes a pair of tuple intervals
    Attempts to merge intervals exclusively (i.e. [(1, 3), (4, 7)] won't be merged)
    Returns a list of one or two tuple intervals
    """
    left, right = sorted(pair)
    distance = right[0] - left[1]

    if distance > 0:            # intervals don't overlap
        return pair
    elif right[1] <= left[1]:   # left interval contains right interval
        return [left,]
    else:                       # intervals overlap
        return [(left[0], right[1])]


def merge_intervals(intervals: list):
    """
    Given list with tuples of intervals in any order.
    Function should merge overlapping intervals into one and
    return a list of tuples which should have only mutually exclusive intervals
    """

    merged = []
    sorted_intervals = sorted(intervals)

    right = sorted_intervals.pop()

    while(sorted_intervals):
        left = sorted_intervals.pop()

        merged_pair = merge_pair([left, right])
        if len(merged_pair) == 2:
            merged.insert(0, right)
            right = left
        else:
            right = merged_pair[0]

    merged.insert(0, right)

    return merged
