import unittest
from merge_intervals import merge_intervals
from is_valid_parentheses import is_valid_parentheses


class TestMergeIntervals(unittest.TestCase):

    def test_unordered_intervals(self):
        self.assertEqual(
            merge_intervals([(1, 3), (4, 6), (1, 7), (9, 10)]),
            [(1, 7), (9, 10)]
        )

    def test_touching_intervals(self):
        self.assertEqual(
            merge_intervals([(1, 2), (4, 6), (1, 3)]),
            [(1, 3), (4, 6)]
        )

    def test_yet_another_interval(self):
        self.assertEqual(
            merge_intervals([(9, 10), (8, 10), (0, 1)]),
            [(0, 1), (8, 10)]
        )


class TestIsValidParentheses(unittest.TestCase):

    valid_cases = ["()", "({})", "[()({})]", "((1, 2,), (3, 4), (,))"]
    invalid_cases = ["[", "{[}]", "{{{{}}}"]

    def test_valid_parentheses(self):
        for valid_case in self.valid_cases:
            self.assertEqual(is_valid_parentheses(valid_case), True)

    def test_invalid_parentheses(self):
        for invalid_case in self.invalid_cases:
            self.assertEqual(is_valid_parentheses(invalid_case), False)


if __name__ == "__main__":
    unittest.main()
