import random
from collections import Counter


def flip():
    return random.randint(0, 1)


def dice_roll():
    """
    Implement random dice roll without using `random` python module.
    The only one source which you can use for random numbers is the `flip` function.
    Should return dice face number (1 to 6)
    """

    while True:
        roll = 0
        for _ in range(3):
            roll <<= 1
            roll += flip()

        if roll < 6:
            return roll + 1


thousand_rolls = Counter([dice_roll() for _ in range(1000)])
