
def is_valid_parentheses(parentheses: str):
    """
    Given string with parentheses expression.
    Function should examine whether the pairs and order of parentheses are correct.
    Possible input characters in string are: `(){}[]`
    """

    counterparts = {
        '(': ')',
        '{': '}',
        '[': ']',
    }

    opening_tokens = counterparts.keys()
    closing_tokens = counterparts.values()

    stack = []

    for token in parentheses:
        if token in opening_tokens:
            closing_token = counterparts[token]
            stack.append(closing_token)
        elif token in closing_tokens:
            # Try to consume corresponding parenthesis from stack or return False
            if stack.pop() != token:
                return False
            pass

    return len(stack) == 0

